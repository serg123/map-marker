create database map_marker;
use map_marker;
create user 'mapmarker' identified by 'MapMarker123';
grant all on map_marker.* to 'mapmarker';

create table collection(
  id int unsigned auto_increment primary key,
  name varchar(300) character set utf8,
  password varchar(100) character set utf8
);
create table map(
  id int unsigned auto_increment primary key,
  name varchar(300) character set utf8,
  url mediumtext character set utf8,
  collection_id int unsigned not null,
  foreign key (collection_id) references collection(id) on delete cascade
);
create table marker(
  id int unsigned auto_increment primary key,
  x DOUBLE,
  y DOUBLE,
  label varchar(100) character set utf8,
  html mediumtext character set utf8,
  map_id int unsigned not null,
  foreign key (map_id) references map(id) on delete cascade,
  answers varchar(1000) character set utf8,
  total_answers int unsigned default 0,
  right_answers int unsigned default 0
);