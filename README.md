## Installation
#### Prerequisites:
- php 5.6
- mysql
- web server (apache preferred)

#### Deployment:
- execute **/private/init.sql** to create user and database (for example `mysql -u root < init.sql`). For custom user or database you can edit codeigniter's configuration. Database config location: /application/config/database.php
- change base url in **/application/config/config.php** (for example  `$config['base_url']='http://example.com'`)

## Used frameworks and libraries
#### Backend:
- [Codeigniter 3.1.6](https://www.codeigniter.com/) ([MIT](https://en.wikipedia.org/wiki/MIT_License) license)

#### Frontend:
- [Bootstrap 4.0.0](http://getbootstrap.com/) ([MIT](https://en.wikipedia.org/wiki/MIT_License) license)
- [jQuery 3.2.1](http://getbootstrap.com/) ([MIT](https://en.wikipedia.org/wiki/MIT_License) license)
- [FontAwesome 4.7.0](http://fontawesome.io/) ([MIT](https://en.wikipedia.org/wiki/MIT_License) license)
- [summernote 0.8.8](https://summernote.org/) ([MIT](https://en.wikipedia.org/wiki/MIT_License) license)
- [Chart.js 2.4.0](http://www.chartjs.org/) ([MIT](https://en.wikipedia.org/wiki/MIT_License) license)

## About
- Visit [wiki](https://gitlab.com/serg123/map-marker/wikis/home) for more info
- Try it [here](http://map-marker.000webhostapp.com/)