function makeTestData(data){
    testIndices={};
    testAnswers={};
    var count=0;
    for(var i=0;i<data.length;i++){
        if(data[i].answers && data[i].answers.length>0){
            testIndices[data[i].id]=[];
            testAnswers[data[i].id]='';
            count++;
        }
    }
    if(count>0)
        $('#testButton').css('display','block');
}

function makeTestHtml(data){
    if(data.answers){
        testId=data.id;
        var html='<br><br>';
        var answers=data.answers.split('\n');
        for(var i=0;i<answers.length;i++){
            var checked = '';
            if(testIndices[data.id].indexOf(i)>=0)
                checked='checked';
            html+='<div class="form-check"><label class="form-check-label"><input class="form-check-input" type="checkbox" value="'+i+'" '+checked+'>';
            html+=answers[i].trim()+'</label></div>';
        }
        html+='</div>';
        return html;
    }
    else{
        testId=0;
        return false;
    }
}

function updateTestData(){
    if(testId){
        var checkboxes=$('#showModal input[type="checkbox"]').get();
        var indices=[];
        var answers=[];
        for(var i=0;i<checkboxes.length;i++){
            if(checkboxes[i].checked) {
                indices.push(Number(checkboxes[i].value));
                answers.push($(checkboxes[i]).parent().html().split('>')[1]);
            }
        }
        testIndices[testId]=indices;
        testAnswers[testId]=answers.join(' ; ');
    }
    $('#showModal').modal('hide');
}

function checkTest(){
    $('#testButton').prop('disabled',true);
    $('#testButton').prop('disabled',true);
    var data={
        id:MAP_ID,
        answers:testIndices,
        text:testAnswers
    }
    data={json:JSON.stringify(data)};
    $.post("test/control", data, function (res) {
        $('#testModal').modal('show');
        var result=JSON.parse(res);
        var html='<ul class="list-group">';
        var total=0; var right=0;
        for(var i=0;i<result.length;i++){
            total++;
            if(result[i][0]) {
                right++;
                html += '<li class="list-group-item list-group-item-success"><i class="fa fa-check mr-4">'+
                '</i><b class="mr-4">' + result[i][1] + '</b><span>' + result[i][2] + '</span></li>';
            }
            else{
                html+='<li class="list-group-item list-group-item-danger"><i class="fa fa-close mr-4"></i><b class="mr-4">'+ result[i][1] +'</b>'+
                '<del class="mr-4">'+ result[i][3] + '</del><span>'+ result[i][2] + '</span></li>';
            }
        }
        html+='</ul>';
        $('#testModal .modal-body').html(html);
        $('#testModal .modal-title').html(TEST_RESULT+right+'/'+total+' ('+Math.round(100*right/total)+'%)');
    }).fail(function(){
        setTimeout(function(){$('#testButton').prop('disabled',false)},3000)
    });
}