$(document).ready(function(){
    setLanguage();
    refresh();
    setInterval(function(){filter(false)},500);
    improveModal('#addModal');
    improveModal('#editModal');
    improveModal('#deleteModal');
});

function refresh(){
    $.get( "collections/get", function( res ) {
        data=JSON.parse(res);
        $('#filter').val('');
        refreshHtml(data);
    });
}

function addCollection(){
    var data={
        name:$('#addModal #name').val(),
        password:$('#addModal #password').val()
    };
    $.post( "collections/add", data, function( res ) {
        refresh();
        $('#addModal').modal('hide');
        $('#addModal #name').val('')
        $('#addModal #password').val('')
    });
}

function editCollection(id) {
    if (id) {
        currentId = id;
        $('#editModal').modal('show');
    }
    else {
        var data={
            id:currentId,
            password:$('#password1').val()
        };
        $.post("collections/login", data, function (res) {
            navigate("maps/edit/0");
        }).fail(function(){
            $('#password1').attr('placeholder','Wrong Password !');
            $('#password1').val('');
        });
    }
}

function deleteCollection(id){
    if(id){
        currentId=id;
        $('#deleteModal').modal('show');
    }
    else {
        var data={
            id:currentId,
            password:$('#password2').val()
        };
        $.post("collections/delete", data, function (res) {
            refresh();
            $('#deleteModal').modal('hide');
            $('#password2').val('');
            $('#password2').attr('placeholder','Password');
        }).fail(function(){
            $('#password2').attr('placeholder','Wrong Password !');
            $('#password2').val('');
        });
    }
}

function refreshHtml(data){
    $('#container').html('');
    for(var i=0;i<data.length;i++) {
        var html= '<div class="input-group mb-3">'+
            '<span class="input-group-btn" style="width:100%">'+
            '<button class="btn btn-outline-primary" type="button" style="width:100%" title="'+OPEN+'" onclick="navigate(\'maps/index/'+data[i].id+'\')"><strong>'+data[i].name+'</strong></button>'+
            '</span>'+
            '<span class="input-group-btn">'+
            '<button class="btn btn-primary pl-3 pr-3" type="button" title="'+EDIT+'" onclick="editCollection('+data[i].id+')"><i class="fa fa-edit"></i></button>'+
            '</span>'+
            '<span class="input-group-btn">'+
            '<button class="btn btn-danger pl-3 pr-3" type="button" title="'+DELETE+'" onclick="deleteCollection('+data[i].id+')"><i class="fa fa-trash"></i></button>'+
            '</span>'+
            '</div>'
        $('#container').append(html);
    }
}

function setLanguage(lang){
    if(!lang){
        lang=getCookie('lang');
        if(!lang) lang='en';
        $('#curLang').html(lang);
    }
    else{
        date = new Date();
        date.setTime(date.getTime()+(100*24*60*60*1000));
        expires = "; expires="+date.toGMTString();
        document.cookie="lang="+lang+expires;
        window.location.reload();
    }
}