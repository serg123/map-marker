function filter(change){
    if(change){
        this.changed=true;
        return;
    }
    else if(this.changed){
        var d=[];
        for(var i=0;i<data.length;i++) {
            if(data[i].name && data[i].name.indexOf($('#filter').val())>=0)
                d.push(data[i]);
        }
        refreshHtml(d);
        this.changed=false;
    }
}

function improveModal(modalSelector,OKSelector){
    $(modalSelector).on('shown.bs.modal',function(){
        $(modalSelector+' input')[0].focus();
    });
    $(modalSelector).on('keydown',function(e){
        if(e.keyCode==13){
            if(OKSelector)
                $(modalSelector+' '+OKSelector).click();
            else
                $(modalSelector+' button').click();
        }
    })
}

function showLoading(parent){
    $(parent).children().hide();
    $(parent).append('<img style="margin-left:100px; margin-right:100px; display: block" src="'+BASE_URL+'/public/img/loading.svg" id="loading"></img>');
}
function hideLoading(parent){
    $(parent).children('#loading').remove();
    $(parent).children().show();
}

function getCookie(name) {
    var matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}