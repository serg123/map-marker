$(document).ready(function(){
    refresh();
    setInterval(function(){loadImage(false)},500);
    setInterval(function(){filter(false)},500);
    improveModal('#addModal');
    showLoading("#imageContainer");
    $("#preview").on('error',function () {
        showLoading("#imageContainer");
    });
});

function refresh(){
    $.get( "maps/get", function( res ) {
        data=JSON.parse(res);
        $('#filter').val('');
        refreshHtml(data);
    });
}

function addMap(){
    var data={
        name:$('#addModal #name').val(),
        url:$("#preview").attr('src')
    };
    $.post( "maps/add", data, function( res ) {
        refresh();
        $('#addModal').modal('hide');
        $('#addModal #name').val('')
        $('#addModal #url').val('')
    });
}

function refreshHtml(data){
    $('#container').html('');
    for(var i=0;i<data.length;i++) {
        var html= '<div class="input-group mb-3">'+
            '<span class="input-group-btn" style="width:100%">'+
            '<button class="btn btn-outline-primary" type="button" style="width:100%" title="'+OPEN+'" onclick="navigate(\'map/index/'+data[i].id+'\')"><strong>'+data[i].name+'</strong></button>'+
            '</span>'+
            '<span class="input-group-btn">'+
            '<button class="btn btn-primary pl-3 pr-3" type="button" title="'+EDIT+'" onclick="navigate(\'map/edit/'+data[i].id+'\')"><i class="fa fa-edit"></i></button>'+
            '</span>'+
            '<span class="input-group-btn">'+
            '<button class="btn btn-danger pl-3 pr-3" type="button" title="'+DELETE+'" onclick="deleteMap('+data[i].id+')"><i class="fa fa-trash"></i></button>'+
            '</span>'+
            '</div>'
        $('#container').append(html);
    }
}

function deleteMap(id){
    var data={ id:id };
    $.post("maps/delete", data, function (res) {
        refresh();
    });
}

function loadImage(file){
    if(!file){
        var url=$("#url").val();
        if(this.url!=url) {
            this.url = url;
            $("#preview").attr("src",url);
                hideLoading("#imageContainer");
            $("#file").val('');
        }
    }
    else{
        var f=$('#file')[0];
        if (f.files && f.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $("#preview").attr("src",e.target.result);
                hideLoading("#imageContainer");
            };
            reader.readAsDataURL(f.files[0]);
        }
    }
}