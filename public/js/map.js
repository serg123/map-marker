$(document).ready(function(){
    showLoading("#imageContainer");
    $('#addModal #summernote').summernote({dialogsInBody: true, lang:LANG});
    $('#addModal #summernote').summernote('code','');
    $('#addModal #summernote').summernote('code','');
    $('#editModal #summernote').summernote({dialogsInBody: true, lang:LANG});
    $('#editModal #summernote').summernote('code','');
    $( window ).on( "orientationchange", function(e) {refresh()});
    $( window ).on( "resize", function(e) {refresh()});
    $('#image').on('click',function(e) {
        var offset = $(this).offset();
        markerX=(e.pageX - offset.left)/this.width;
        markerY=(e.pageY - offset.top)/this.height;
        markerId=0;
        $('#addModal').modal('show');
    });
    $('#image').on('load',function(){
        hideLoading("#imageContainer");
        refresh();
    });
    loadImage();
});

function refresh(){
    $.get( "map/get/"+MAP_ID, function( res ) {
        var data=JSON.parse(res);
        $('#container').html('');
        var offset=$('#image').offset(); var iw=$('#image').width(); var ih=$('#image').height();
        var MARKER_SIZE=40
        for(var i=0;i<data.length;i++) {
            var left=data[i].x*iw+offset.left-MARKER_SIZE*0.28;
            var top=data[i].y*ih+offset.top-MARKER_SIZE*0.94;
            var html='<div class="fa fa-map-marker" style="cursor:pointer; position:absolute; left:'+left+'px; top:'+top+'px; '+
                'color:#ff3333; text-shadow: 2px -2px 0 #990000; font-size:'+MARKER_SIZE+'px" onclick="onMarkerClick('+data[i].id+')"></div>'+
                '<h5><span class="badge badge-light" style="cursor:default; position:absolute; left:'+left+'px; top:'+(top-MARKER_SIZE/2)+'px;">'+data[i].label+'</span></h5>';
            $('#container').append(html);
        }
    });
}

function loadImage(){
    $.get( "map/image/"+MAP_ID, function( res ) {
        var data=JSON.parse(res);
        $('#image').attr('src',data);
    });
}

function onMarkerClick(id){
    $('#editModal').modal('show');
    showLoading("#editModal .modal-body");
    $.get( "map/marker/"+id, function( res ) {
        var data=JSON.parse(res);
        markerId=id;
        markerX=data.x;
        markerY=data.y;
        $('#editModal #summernote').summernote('code',data.html);
        $('#editModal #label').val(data.label);
        $('#editModal #answers').val(data.answers);
        hideLoading("#editModal .modal-body");
    });
}

function addMarker(){
    var data={
        x:markerX,
        y:markerY,
        html:$('#addModal #summernote').summernote('code'),
        label:$('#addModal #label').val(),
        answers:$('#addModal #answers').val(),
        map_id:MAP_ID
    };
    $.post("map/add", data, function (res) {
        $('#addModal').modal('hide');
        refresh();
    });
}

function deleteMarker(){
    var data={ id:markerId };
    $.post("map/delete", data, function (res) {
        $('#editModal').modal('hide');
        refresh();
    });
}

function updateMarker(){
    var data={ id:markerId };
    $.post("map/delete", data, function (res) {});
    data={
        x:markerX,
        y:markerY,
        html:$('#editModal #summernote').summernote('code'),
        label:$('#editModal #label').val(),
        answers:$('#editModal #answers').val(),
        map_id:MAP_ID
    };
    $.post("map/add", data, function (res) {
        $('#editModal').modal('hide');
        refresh();
    });
}