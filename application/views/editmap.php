<?php $lang=get_cookie('lang');
if(!$lang) $lang='en'?>
<div class="container-fluid">
<div class="row">
    <div class="col-12 p-0">
        <div class="d-flex p-2 alert-primary">
            <button class="btn btn-success pl-4 pr-4" title="<?php echo trn($lang,'back');?>" onclick="navigate('maps/edit/0')"><i class="fa fa-arrow-left"></i></button>
            <h4 class="text-primary mx-auto"><?php echo $map->name ?></h4>
            <button class="btn btn-success pl-4 pr-4" title="<?php echo trn($lang,'statistics');?>" onclick="navigate('test/statistics/<?php echo $map->id?>')"><i class="fa fa-bar-chart"></i></button>
        </div>
        <div id="imageContainer">
            <img class="col-12 p-0" id="image">
        </div>
    </div>
</div>
</div>

<!--markers go here-->
<div id="container"></div>

<!--new marker modal-->
<div class="modal fade" id="addModal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header alert-primary">
                <h5 class="modal-title"><?php echo trn($lang,'add_marker');?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="<?php echo trn($lang,'label');?>" id="label">
                </div>
                <div class="form-group">
                    <div id="summernote"></div>
                </div>
                <div class="form-group">
                    <textarea class="form-control" placeholder="<?php echo trn($lang,'answers');?>" id="answers"></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="addMarker()" id="btnOK"><?php echo trn($lang,'ok');?></button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo trn($lang,'cancel');?></button>
            </div>
        </div>
    </div>
</div>

<!--edit marker modal-->
<div class="modal fade" id="editModal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header alert-primary">
                <h5 class="modal-title"><?php echo trn($lang,'edit_marker');?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="<?php echo trn($lang,'label');?>" id="label">
                </div>
                <div class="form-group">
                    <div id="summernote"></div>
                </div>
                <div class="form-group">
                    <textarea class="form-control" placeholder="<?php echo trn($lang,'answers');?>" id="answers"></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="updateMarker()" id="btnOK"><?php echo trn($lang,'ok');?></button>
                <button type="button" class="btn btn-danger" onclick="deleteMarker()"><?php echo trn($lang,'delete');?></button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo trn($lang,'cancel');?></button>
            </div>
        </div>
    </div>
</div>

<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.js"></script>
<script>
    MAP_ID=<?php echo $map->id?>;
    LANG='<?php echo $lang?>';
    LANG=LANG+'-'+LANG.toUpperCase();
</script>
<script src="<?php echo base_url();?>/public/lang/summernote-ru-RU.js"></script>
<script src="<?php echo base_url();?>/public/js/map.js"></script>