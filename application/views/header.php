<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>map-marker</title>
    <base href="<?php echo site_url();?>/">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.css" rel="stylesheet">
	<script>
        //IE SUPPORT:
        $.ajaxSetup({ cache: false });
        function navigate(url){document.location.href=$('base').prop('href')+url;}
        BASE_URL='<?php echo base_url();?>';
    </script>
    <style>
        .btn-outline-primary {
            white-space: normal !important;
            word-wrap: break-word;
        }
        html {
            -ms-overflow-style: -ms-autohiding-scrollbar;
        }
        .fa {
            cursor: default;
        }
    </style>
    <script src="<?php echo base_url();?>/public/js/other.js"></script>
</head>
<body>