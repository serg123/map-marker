<?php $lang=get_cookie('lang');
if(!$lang) $lang='en'?>
<div class="container-fluid">
<div class="row">
    <div class="col-12 p-0">
        <div class="d-flex p-2 alert-primary">
            <button class="btn btn-success pl-4 pr-4" title="<?php echo trn($lang,'back');?>" onclick="navigate('map/edit/<?php echo $map->id?>')"><i class="fa fa-arrow-left"></i></button>
            <h3 class="text-primary mx-auto"><?php echo $map->name ?></h3>
            <button class="btn btn-success pl-4 pr-4" id="reset" title="<?php echo trn($lang,'clear');?>" onclick="reset(<?php echo $map->id?>)"><i class="fa fa-close"></i></button>
        </div>
        <div class="m-3" style="height:80vh;width:95vw">
            <canvas id="chart"></canvas>
        </div>
    </div>
</div>
</div>


<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
<script>
    CHART_TITLE='<?php echo trn($lang,'chart_title');?>';
    MAP_ID=<?php echo $map->id?>

    $(document).ready(function(){
        var canvas=$('#chart')[0];
        var ctx=canvas.getContext('2d');
        chart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: [],
                datasets: [{
                    label: '',
                    backgroundColor: 'rgb(50, 200, 100)',
                    data: []
                }]
            },
            options: {responsive:true,maintainAspectRatio:false,
                scales: {
                    yAxes : [{
                        ticks : {
                            max : 100,
                            min : 0
                        }
                    }]
                }
            }
        });
        setInterval(refresh,3000);
        refresh();
    });

    function refresh(){
        $.get( "test/stat/"+MAP_ID, function( res ) {
            var data=JSON.parse(res);
            data=process(data);
            chart.data.datasets[0].data=data.data;
            chart.data.datasets[0].label=data.label;
            chart.data.labels = data.labels;
            chart.update();
        });
    }

    function reset(id){
        $.get("test/reset/"+id, function(){refresh()});
    }

    function process(d) {
        var labels=[];
        var data=[];
        var sum=0;
        for(var i=0;i<d.length;i++){
            var percentage=d[i].right_answers/d[i].total_answers;
            if(isNaN(percentage)) continue;
            labels.push(d[i].label);
            data.push(Math.round(percentage*1000)/10);
            sum+=percentage;
        }
        sum=Math.round(sum/data.length*1000)/10;
        return {labels:labels, data:data, label:CHART_TITLE+' ('+sum+'%)'}
    }
</script>