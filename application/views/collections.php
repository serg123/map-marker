<?php $lang=get_cookie('lang');
if(!$lang) $lang='en'?>
<div class="container">
<div class="row mt-sm-4">
    <div class="col-12 col-sm-10 col-md-8 col-lg-6 mx-auto p-0">
<!--        header with add button and filter-->
        <div class="d-flex alert-primary p-4 border border-primary">
            <button class="btn btn-primary pl-4 pr-4" data-toggle="modal" data-target="#addModal" title="<?php echo trn($lang,'add');?>"><i class="fa fa-plus"></i></button>
            <input type="text" class="form-control ml-4" placeholder="<?php echo trn($lang,'filter');?>" onkeyup="filter(true)" id="filter">
            <div class="dropdown pl-4">
                <button class="btn btn-light dropdown-toggle" type="button" id="curLang" data-toggle="dropdown">
                    en
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                    <button class="dropdown-item" type="button" onclick="setLanguage('en')">english</button>
                    <button class="dropdown-item" type="button" onclick="setLanguage('ru')">русский</button>
                    <button class="dropdown-item" type="button" onclick="setLanguage('et')">eesti</button>
                </div>
            </div>
        </div>
        <div class="p-4 border border-secondary border-top-0" id="container">
            <!--        insert content here-->
        </div>
    </div>
</div>
</div>

<!--new modal-->
<div class="modal fade" id="addModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header alert-primary">
                <h5 class="modal-title"><?php echo trn($lang,'add_col');?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="<?php echo trn($lang,'name');?>" id="name">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" placeholder="<?php echo trn($lang,'password');?>" id="password">
                    </div>
                </form>
                </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="addCollection()"><?php echo trn($lang,'ok');?></button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo trn($lang,'cancel');?></button>
            </div>
        </div>
    </div>
</div>
<!--edit confirmation modal-->
<div class="modal fade" id="editModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header alert-primary">
                <h5 class="modal-title"><?php echo trn($lang,'edit_col');?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <input type="password" class="form-control" placeholder="<?php echo trn($lang,'password');?>" id="password1">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="editCollection()"><?php echo trn($lang,'ok');?></button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo trn($lang,'cancel');?></button>
            </div>
        </div>
    </div>
</div>
<!--delete confirmation modal-->
<div class="modal fade" id="deleteModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header alert-danger">
                <h5 class="modal-title"><?php echo trn($lang,'delete_col');?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <input type="password" class="form-control" placeholder="<?php echo trn($lang,'password');?>" id="password2">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="deleteCollection()"><?php echo trn($lang,'ok');?></button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo trn($lang,'cancel');?></button>
            </div>
        </div>
    </div>
</div>

<script>
    EDIT='<?php echo trn($lang,'edit');?>';
    OPEN='<?php echo trn($lang,'open');?>';
    DELETE='<?php echo trn($lang,'delete');?>';
</script>
<script src="<?php echo base_url();?>/public/js/collections.js"></script>