<?php $lang=get_cookie('lang');
if(!$lang) $lang='en'?>
<div class="container-fluid">
<div class="row">
    <div class="col-12 p-0">
        <div class="d-flex p-2 alert-primary">
            <button class="btn btn-success pl-4 pr-4" title="<?php echo trn($lang,'back');?>" onclick="navigate('maps/edit/<?php echo $map->collection_id?>')"><i class="fa fa-arrow-left"></i></button>
            <h3 class="text-primary mx-auto"><?php echo $map->name ?></h3>
            <button class="btn btn-success pl-4 pr-4" onclick="checkTest()" title="<?php echo trn($lang,'check');?>" id="testButton" id="testButton" style="display:none"><i class="fa fa-check"></i></button>
        </div>
        <div id="imageContainer">
        <img class="col-12 p-0" id="image">
        </div>
    </div>
</div>
</div>

<!--markers go here-->
<div id="container"></div>

<!--show marker's html modal-->
<div class="modal fade" id="showModal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header alert-primary">
                <h5 class="modal-title"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer" style="display:none">
                <button type="button" class="btn btn-primary" onclick="updateTestData()" ><?php echo trn($lang,'ok');?></button>
            </div>
        </div>
    </div>
</div>

<!--test results modal-->
<div class="modal fade" id="testModal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header alert-primary">
                <h5 class="modal-title"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body"></div>
        </div>
    </div>
</div>

<script src="<?php echo base_url();?>/public/js/test.js"></script>
<script>
    MAP_ID=<?php echo $map->id?>;
    TEST_RESULT='<?php echo trn($lang,'test_result');?>';

    $(document).ready(function(){
        showLoading("#imageContainer");
        $( window ).on( "orientationchange", function(e) {refresh()});
        $( window ).on( "resize", function(e) {refresh()});
        $('#image').on('load',function(){
            hideLoading("#imageContainer");
            refresh();
        });
        loadImage();
    });

    function loadImage(){
        $.get( "map/image/"+MAP_ID, function( res ) {
            var data=JSON.parse(res);
            $('#image').attr('src',data);
        });
    }

    function refresh(){
        $.get( "map/get/"+MAP_ID, function( res ) {
            var data=JSON.parse(res);
            makeTestData(data);
            $('#container').html('');
            var offset=$('#image').offset(); var iw=$('#image').width(); var ih=$('#image').height();
            var MARKER_SIZE=40
            for(var i=0;i<data.length;i++) {
                var left=data[i].x*iw+offset.left-MARKER_SIZE*0.28;
                var top=data[i].y*ih+offset.top-MARKER_SIZE*0.94;
                var html='<div class="fa fa-map-marker" style="cursor:pointer; position:absolute; left:'+left+'px; top:'+top+'px; '+
                    'color:#ff3333; text-shadow: 2px -2px 0 #990000; font-size:'+MARKER_SIZE+'px" onclick="onMarkerClick('+data[i].id+')"></div>'+
                    '<h5><span class="badge badge-light" style="cursor:default; position:absolute; left:'+left+'px; top:'+(top-MARKER_SIZE/2)+'px;">'+data[i].label+'</span></h5>';
                $('#container').append(html);
            }
        });
    }

    function onMarkerClick(id){
        $('#showModal').modal('show');
        showLoading("#showModal .modal-content");
        $.get( "map/marker/"+id, function( res ) {
            var data=JSON.parse(res);
            markerId=id;
            markerX=data.x;
            markerY=data.y;
            $('#showModal .modal-body').html(data.html);
            $('#showModal .modal-title').html(data.label);
            var html=makeTestHtml(data);
            if(html){
                $('#showModal .modal-body').append(html);
                $('#showModal .modal-footer').css('display','flex');
            }
            else
                $('#showModal .modal-footer').css('display','none');
            hideLoading("#showModal .modal-content");
        });
    }
</script>