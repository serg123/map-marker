<?php $lang=get_cookie('lang');
if(!$lang) $lang='en'?>
<div class="container">
<div class="row mt-sm-4">
    <div class="col-12 col-sm-10 col-md-8 col-lg-6 mx-auto p-0">
<!--        header with add, back buttons and filter-->
        <div class="d-flex alert-primary p-4 border border-primary">
            <button class="btn btn-success pl-4 pr-4" title="<?php echo trn($lang,'back');?>" onclick="navigate('collections')"><i class="fa fa-arrow-left"></i></button>
            <input type="text" class="form-control ml-4" placeholder="<?php echo trn($lang,'filter');?>" onkeyup="filter(true)" id="filter">
        </div>
        <div class="p-4 border border-secondary border-top-0 text-primary text-center">
            <h4><?php echo $name ?><h4>
        </div>
        <div class="p-4 border border-secondary border-top-0" id="container">
            <!--        insert content here-->
        </div>
    </div>
</div>
</div>

<script>
    json='<?php echo json_encode($data); ?>';
    data=JSON.parse(json);
    $(document).ready(function() {
        refreshHtml(data)
        setInterval(function(){filter(false)},500);
    });
    function refreshHtml(data){
        $('#container').html('');
        for(var i=0;i<data.length;i++) {
            var html= '<div class="input-group mb-3">'+
                '<span class="input-group-btn" style="width:100%">'+
                '<button class="btn btn-outline-primary" type="button" style="width:100%" title="<?php echo trn($lang,'open');?>" onclick="navigate(\'map/index/'+data[i].id+'\')"><strong>'+data[i].name+'</strong></button>'+
                '</span>'
                '</div>'
            $('#container').append(html);
        }
    }
</script>