<?php $lang=get_cookie('lang');
if(!$lang) $lang='en'?>
<div class="container">
<div class="row mt-sm-4">
    <div class="col-12 col-sm-10 col-md-8 col-lg-6 mx-auto p-0">
<!--        header with add, back buttons and filter-->
        <div class="d-flex alert-primary p-4 border border-primary">
            <button class="btn btn-success pl-4 pr-4" title="<?php echo trn($lang,'back');?>" onclick="navigate('collections/logout')"><i class="fa fa-arrow-left"></i></button>
            <button class="btn btn-primary pl-4 pr-4 ml-4" title="<?php echo trn($lang,'add');?>" data-toggle="modal" data-target="#addModal"><i class="fa fa-plus"></i></button>
            <input type="text" class="form-control ml-4" placeholder="<?php echo trn($lang,'filter');?>" onkeyup="filter(true)" id="filter">
        </div>
        <div class="p-4 border border-secondary border-top-0 text-primary text-center">
            <h4><?php echo $name ?><h4>
        </div>
        <div class="p-4 border border-secondary border-top-0" id="container">
            <!--        insert content here-->
        </div>
    </div>
</div>
</div>

<!--new modal-->
<div class="modal fade" id="addModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header alert-primary">
                <h5 class="modal-title"><?php echo trn($lang,'add_map');?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" style="max-height:600px">
                <form>
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="<?php echo trn($lang,'name');?>" id="name">
                    </div>
                    <div class="form-group">
                        <input type="button" class="btn btn-secodary" value="<?php echo trn($lang,'file');?>" onclick="document.getElementById('file').click();" />
                        <input type="file" style="display:none;" id="file" class="form-control-file" onchange="loadImage(true)">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="<?php echo trn($lang,'url');?>" id="url">
                    </div>
                </form>
                <div id="imageContainer">
                <img src="" id="preview" class="img-thumbnail" style="max-height:400px">
                </div>
                </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="addMap()"><?php echo trn($lang,'ok');?></button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo trn($lang,'cancel');?></button>
            </div>
        </div>
    </div>
</div>

<script>
    EDIT='<?php echo trn($lang,'edit');?>';
    OPEN='<?php echo trn($lang,'open');?>';
    DELETE='<?php echo trn($lang,'delete');?>';
</script>
<script src="<?php echo base_url();?>/public/js/maps.js"></script>
