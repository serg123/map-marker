<?php

class Map extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->model('map_model');
        $this->load->model('marker_model');
    }
    public function index($id){
        $map=$this->map_model->get($id);
        $this->load->view('header');
        $this->load->view('map',['map'=>$map]);
        $this->load->view('footer');
    }
	public function edit($id){
        authorize($this->session);
        $map=$this->map_model->get($id);
        $this->load->view('header');
        $this->load->view('editmap',['map'=>$map]);
        $this->load->view('footer');
	}
    public function get($id){
        $data = $this->marker_model->all($id);
        echo json_encode($data);
    }
    public function image($id){
        $map=$this->map_model->getUrl($id);
        echo json_encode($map->url);
    }
    public function marker($id){
        $data = $this->marker_model->get($id);
        echo json_encode($data);
    }
	public function add(){
        authorize($this->session);
        $html = $this->input->post('html');
        $x = $this->input->post('x');
        $y = $this->input->post('y');
        $map_id=$this->input->post('map_id');
        $label=$this->input->post('label');
        $answers=$this->input->post('answers');
        if($answers && strlen($answers)>0)
            $this->marker_model->insert($label, $html, $x, $y, $map_id, $answers);
        else
            $this->marker_model->insert($label, $html, $x, $y, $map_id);
    }
    public function delete(){
        authorize($this->session);
        $id = $this->input->post('id');
        $this->marker_model->delete($id);
    }
}
