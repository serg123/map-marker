<?php

class Maps extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->model('map_model');
        $this->load->model('collection_model');
    }
    public function index($id){
        $data = $this->map_model->all($id);
        $col=$this->collection_model->get($id);
        $this->load->view('header');
        $this->load->view('maps',['data'=>$data, 'name'=>$col->name]);
        $this->load->view('footer');
    }
	public function edit($id){
        $s=$this->session;
        if($s->has_userdata('id') && $s->has_userdata('name')) {
            $this->load->view('header');
            $this->load->view('editmaps', ['name' => $this->session->name]);
            $this->load->view('footer');
        }
        else
            redirect('maps/index/'.$id, 'refresh');

	}
    public function get(){
        $collection_id=$this->session->id;
        $data = $this->map_model->all($collection_id);
        echo json_encode($data);
    }
	public function add(){
        authorize($this->session);
        $name = $this->input->post('name');
        $url = $this->input->post('url');
        $collection_id=$this->session->id;
        $this->map_model->insert($name, $url, $collection_id);
    }
    public function delete(){
        authorize($this->session);
        $id = $this->input->post('id');
        $this->map_model->delete($id);
    }
}
