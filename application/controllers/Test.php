<?php

class Test extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->model('map_model');
        $this->load->model('marker_model');
    }
    public function control(){
        $json = json_decode($this->input->post('json'));
        $answers = $json->answers;
        $actual = $this->marker_model->all($json->id);
        $result=array();
        foreach($actual as $act){
            if(!$act->answers)
                continue;
            $id=$act->id;
            $answer=$answers->$id;
            $right=$this->rightAnswers($act->answers);
            $res=$this->check($answer,$right);
            $result[]=array($res,$act->label,implode($right,' ; '),$json->text->$id);
            $this->marker_model->incAnswer($id,$res);
        }
        echo json_encode($result);
    }

    private function rightAnswers($txt){
        $lines = explode("\n", $txt);
        $result=array();
        $i=0;
        foreach($lines as $l){
            if(strlen($l)>0 && $l[0]==' ')
                $result[$i]=trim($l);
            $i++;
        }
        return $result;
    }
    private function check($answer,$right){
        if(count($answer)!=count($right))
            return false;
        foreach ($right as $key => $value){
            if(!in_array((int)$key,$answer))
                return false;
        }
        return true;
    }

    public function statistics($id){
        authorize($this->session);
        $map=$this->map_model->get($id);
        $this->load->view('header');
        $this->load->view('statistics',['map'=>$map]);
        $this->load->view('footer');
    }
    public function reset($id){
        authorize($this->session);
        $this->marker_model->resAnswers($id);
    }
    public function stat($id){
        $data=$this->marker_model->statAnswers($id);
        echo json_encode($data);
    }
}
