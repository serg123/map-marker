<?php

class Collections extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->model('collection_model');
    }
	public function index(){
        $this->load->view('header');
		$this->load->view('collections');
        $this->load->view('footer');
	}
    public function get(){
        $data=$this->collection_model->all();
        echo json_encode($data);
    }
	public function add(){
	    $name=$this->input->post('name');
        $password=$this->input->post('password');
        $this->collection_model->insert($name, $password);

    }
    public function delete(){
        $id=$this->input->post('id');
        $password=$this->input->post('password');
        if($this->collection_model->auth($id,$password))
            $this->collection_model->delete($id);
        else
            show_404();
    }
    public function login(){
        $id=$this->input->post('id');
        $password=$this->input->post('password');
        $result=$this->collection_model->auth($id,$password);
        if(count($result)>0) {
            $this->session->set_userdata('id', $id);
            $this->session->set_userdata('name', $result[0]->name);
        }
        else
            show_404();
    }
    public function logout(){
        session_destroy();
        redirect('/', 'refresh');
    }
}
