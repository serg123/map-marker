<?php
class Collection_model extends CI_Model {
    private $TABLE;
    public function __construct(){
        $this->load->database();
        $this->TABLE='collection';
    }
    public function insert($name, $password){
        $data=array('name'=>$name,'password'=>$password);
        $this->db->insert($this->TABLE, $data);
    }
    public function all(){
        $query=$this->db->select('id,name')->get($this->TABLE);
        return $query->result();
    }
    public function get($id){
        $query=$this->db->select('id,name')->where('id',$id)->get($this->TABLE);
        return $query->result()[0];
    }
    public function delete($id){
        $this->db->where('id', $id)->delete($this->TABLE);
    }
    public function auth($id,$password){
        $this->db->where('id', $id);
        $this->db->where('password', $password);
        return $this->db->get($this->TABLE)->result();
    }
}