<?php
class Marker_model extends CI_Model {
    private $TABLE;
    public function __construct(){
        $this->load->database();
        $this->TABLE='marker';
    }
    public function insert($label, $html, $x, $y, $map_id, $answers=null){
        $data=array('label'=>$label, 'html'=>$html,'x'=>$x, 'y'=>$y, 'map_id'=>$map_id, 'answers'=>$answers);
        $this->db->insert($this->TABLE, $data);
    }
    public function all($id){
        $query=$this->db->select('id,x,y,label,map_id,answers,total_answers,right_answers')->where('map_id',$id)->get($this->TABLE);
        return $query->result();
    }
    public function get($id){
        $query=$this->db->where('id',$id)->get($this->TABLE);
        return $query->result()[0];
    }
    public function delete($id){
        $this->db->where('id', $id)->delete($this->TABLE);
    }

    public function incAnswer($id,$isValid){
        $this->db->where('id', $id);
        $this->db->set('total_answers', 'total_answers+1', false);
        if($isValid)
            $this->db->set('right_answers', 'right_answers+1', false);
        $this->db->update($this->TABLE);
    }
    public function resAnswers($id){
        $this->db->where('map_id', $id);
        $this->db->set('total_answers', 0);
        $this->db->set('right_answers', 0);
        $this->db->update($this->TABLE);
    }
    public function statAnswers($id){
        $query=$this->db->select('label,,total_answers,right_answers')->where('map_id',$id)->get($this->TABLE);
        return $query->result();
    }
}