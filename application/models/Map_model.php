<?php
class Map_model extends CI_Model {
    private $TABLE;
    public function __construct(){
        $this->load->database();
        $this->TABLE='map';
    }
    public function insert($name, $url, $collection_id){
        $data=array('name'=>$name,'url'=>$url, 'collection_id'=>$collection_id);
        $this->db->insert($this->TABLE, $data);
    }
    public function all($id){
        $query=$this->db->select('id,name,collection_id')->where('collection_id',$id)->get($this->TABLE);
        return $query->result();
    }
    public function get($id){
        $query=$this->db->select('id,name,collection_id')->where('id',$id)->get($this->TABLE);
        return $query->result()[0];
    }
    public function getUrl($id){
        $query=$this->db->select('url')->where('id',$id)->get($this->TABLE);
        return $query->result()[0];
    }
    public function delete($id){
        $this->db->where('id', $id)->delete($this->TABLE);
    }
}