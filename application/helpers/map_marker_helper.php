<?php
define('en_chart_title', 'Right Answers Percentage');
define('en_back', 'back');
define('en_clear', 'clear');
define('en_add', 'add');
define('en_delete', 'Delete');
define('en_edit', 'edit');
define('en_open', 'open');
define('en_filter', 'Filter');
define('en_add_col', 'Add a new collection');
define('en_name', 'Name');
define('en_password', 'Password');
define('en_ok', 'OK');
define('en_cancel', 'Cancel');
define('en_check', 'check');
define('en_add_map', 'Add a new map');
define('en_url', 'URL');
define('en_file', 'Load from disk');
define('en_statistics', 'statistics');
define('en_edit_marker', 'Edit marker');
define('en_add_marker', 'Add marker');
define('en_label', 'Label');
define('en_answers', 'Not mandatory! Each variant on separate line, right starts with space');
define('en_test_result', 'Test result: ');
define('en_edit_col', 'Password required to edit');
define('en_delete_col', 'Password required to delete');

define('ru_chart_title', 'Процент Верных Ответов');
define('ru_back', 'назад');
define('ru_clear', 'очистить');
define('ru_add', 'добавить');
define('ru_delete', 'Удалить');
define('ru_edit', 'редактировать');
define('ru_open', 'открыть');
define('ru_filter', 'Фильтр');
define('ru_add_col', 'Добавить новую коллекцию');
define('ru_name', 'Имя');
define('ru_password', 'Пароль');
define('ru_ok', 'ОК');
define('ru_cancel', 'Отмена');
define('ru_check', 'проверить');
define('ru_add_map', 'Добавить новую карту');
define('ru_url', 'Адрес');
define('ru_file', 'Загрузить с диска');
define('ru_statistics', 'статистика');
define('ru_edit_marker', 'Редактирование маркера');
define('ru_add_marker', 'Добавить маркер');
define('ru_label', 'Метка');
define('ru_answers', 'Не обязательно! Каждый вариант на своей строке, перед верным пробел');
define('ru_test_result', 'Результат теста: ');
define('ru_edit_col', 'Для редактирования введите пароль');
define('ru_delete_col', 'Для удаления введите пароль');

define('et_chart_title', 'Õigete Vastuste Protsent');
define('et_back', 'tagasi');
define('et_clear', 'kustuta');
define('et_add', 'lisa');
define('et_delete', 'Kustuta');
define('et_edit', 'redakteeri');
define('et_open', 'ava');
define('et_filter', 'Filter');
define('et_add_col', 'Lisa uus kollektsioon');
define('et_name', 'Nimi');
define('et_password', 'Salasõna');
define('et_ok', 'OK');
define('et_cancel', 'Sulge');
define('et_check', 'kontrolli');
define('et_add_map', 'Lisa uus kaart');
define('et_url', 'Aadress');
define('et_file', 'Laadi kettalt');
define('et_statistics', 'statistika');
define('et_edit_marker', 'Markeri redakteerimine');
define('et_add_marker', 'Uus marker');
define('et_label', 'Silt');
define('et_answers', 'Pole kohustuslik! Iga variant eraldi rial, algab otse tühikuga');
define('et_test_result', 'Testi tulemus: ');
define('et_edit_col', 'Salasõna on vaja redakteerimiseks');
define('et_delete_col', 'Salasõna on vaja kustutamiseks');


function authorize($session){
    if(!($session->has_userdata('id') && $session->has_userdata('name')))
        show_404();
}

function trn($lang,$key){
    return constant($lang.'_'.$key);
}